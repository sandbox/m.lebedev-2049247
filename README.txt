-------------------------------------------------------------------------------
Prev next menu for Drupal 7.x
-------------------------------------------------------------------------------
Description:

Adds a block with the previous or next menu item for any menu.
This is very useful for providing navigational links to the user without the
expensive queries required to dynamically deduce such information on the fly.
